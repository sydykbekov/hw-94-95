import React from 'react';
import {Button, Panel} from "react-bootstrap";
import PropTypes from 'prop-types';

const ProductListItem = props => {
    return (
        <Panel>
            <Panel.Body>
                {props.remove && <Button className="pull-right" bsStyle="danger" onClick={() => props.remove(props.id)}>Remove</Button>}
                Название: {props.title} <br/>
                Продолжительность: {props.duration} <br/>
                <strong>Дата события: {props.date}</strong>
            </Panel.Body>
        </Panel>
    );
};

ProductListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    duration: PropTypes.string.isRequired
};

export default ProductListItem;