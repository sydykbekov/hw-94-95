import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";

class ProductForm extends Component {
    state = {
        title: '',
        date: '',
        duration: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Event title"
                    type="text"
                    value={this.state.title}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="date"
                    title="Event date"
                    type="date"
                    value={this.state.date}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="duration"
                    title="Event duration"
                    type="text"
                    value={this.state.duration}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default ProductForm;
