import React from 'react';
import {Route, Switch} from "react-router-dom";

import NewProduct from "./containers/NewProduct/NewProduct";
import Register from "./containers/Register/Register";
import Products from "./containers/Products/Products";
import Login from "./containers/Login/Login";
import Share from './containers/Share/Share';
import Friends from './containers/Friends/Friends';

const Routes = ({user}) => (
  <Switch>
    <Route path="/" exact component={Products}/>
    <Route path="/products/new" exact component={NewProduct}/>
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
    <Route path="/share" exact component={Share}/>
    <Route path="/friends" exact component={Friends}/>
  </Switch>
);

export default Routes;