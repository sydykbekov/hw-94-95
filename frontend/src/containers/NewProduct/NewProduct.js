import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct} from "../../store/actions/products";
import {fetchCategories} from "../../store/actions/categories";

class NewProduct extends Component {
    componentDidMount() {
        this.props.fetchCategories();
    }

    createProduct = productData => {
        this.props.onProductCreated(productData, this.props.token);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New product</PageHeader>
                <ProductForm
                    onSubmit={this.createProduct}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onProductCreated: (productData, token) => dispatch(createProduct(productData, token)),
    fetchCategories: () => dispatch(fetchCategories())
});

const mapStateToProps = state => ({
    categories: state.categories.allCategories,
    token: state.users.token
});

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);