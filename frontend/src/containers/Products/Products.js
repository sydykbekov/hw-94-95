import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {fetchProducts, removeEvent} from "../../store/actions/products";
import {Link} from "react-router-dom";

import ProductListItem from '../../components/ProductListItem/ProductListItem';

class Products extends Component {
    componentDidMount() {
        if (this.props.user) {
            this.props.onFetchProducts(this.props.token);
        }
    }

    remove = eventID => {
        this.props.removeEvent(eventID, this.props.user._id);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Products
                    <Link to="/products/new">
                        <Button bsStyle="primary" className="pull-right">
                            Add product
                        </Button>
                    </Link>
                </PageHeader>

                {this.props.user && this.props.products.map(product => (
                    <ProductListItem
                        key={product._id}
                        id={product._id}
                        title={product.title}
                        date={product.date}
                        duration={product.duration}
                        remove={product.userID === this.props.user._id && this.remove}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.products.products,
        user: state.users.user,
        token: state.users.token
    }
};

const mapDispatchToProps = dispatch => ({
    onFetchProducts: token => dispatch(fetchProducts(token)),
    removeEvent: (id, userID) => dispatch(removeEvent(id, userID))
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);