import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, PageHeader, Panel} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {searchUser, share} from "../../store/actions/products";

class Share extends Component {
    state = {
        email: ''
    };

    inputChangeHandler = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    share = (friend, token) => {
        this.props.share(friend, token);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Share with:</PageHeader>
                <Panel>
                    <Panel.Body>
                        <FormElement
                            propertyName="email"
                            title="User email"
                            type="email"
                            value={this.state.email}
                            changeHandler={this.inputChangeHandler}
                            required
                        />
                        <Button bsStyle="primary" onClick={() => this.props.search(this.state.email)}>Search user</Button>
                    </Panel.Body>

                </Panel>
                {this.props.friend && <Panel>
                    <Panel.Body>
                        <b>Username: </b>{this.props.friend.username}
                        <Button className="pull-right" bsStyle="success" onClick={() => this.share(this.props.friend, this.props.token)}>Add friend</Button>
                    </Panel.Body>
                </Panel>}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    friend: state.products.shareWith,
    user: state.users.user,
    token: state.users.token
});

const mapDispatchToProps = dispatch => ({
    search: email => dispatch(searchUser(email)),
    share: (friend, token) => dispatch(share(friend, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(Share);