import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, PageHeader, Panel} from "react-bootstrap";
import {unsubscribe} from "../../store/actions/products";

class Friends extends Component {
    render() {
        return (
            <Fragment>
                <PageHeader>Friends</PageHeader>
                {this.props.user.subscribers.map(friend =>
                    <Panel key={friend._id}>
                        <Panel.Body>
                            {friend.username}
                            <Button className="pull-right" bsStyle="warning"
                                    onClick={() => this.props.unsubscribe(friend._id, this.props.token)}>Unsubscribe</Button>
                        </Panel.Body>
                    </Panel>
                )}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    token: state.users.token
});

const mapDispatchToProps = dispatch => ({
    unsubscribe: (id, token) => dispatch(unsubscribe(id, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(Friends);