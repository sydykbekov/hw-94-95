import axios from '../../axios-api';
import {push} from "react-router-redux";
import {NotificationManager} from 'react-notifications';
import {CREATE_PRODUCT_SUCCESS, FETCH_PRODUCTS_SUCCESS, SHARE_WITH_USER} from "./actionTypes";
import {loginUserSuccess} from "./users";

export const fetchProductsSuccess = products => {
    return {type: FETCH_PRODUCTS_SUCCESS, products};
};

const shareWith = user => {
    return {type: SHARE_WITH_USER, user};
};

export const fetchProducts = (token) => {
    const headers = {"Token": token};
    return dispatch => {
        axios.get(`/events`, {headers}).then(response => {
                dispatch(fetchProductsSuccess(response.data))
            }
        );
    }
};

export const createProductSuccess = () => {
    return {type: CREATE_PRODUCT_SUCCESS};
};

export const createProduct = (event, token) => {
    return dispatch => {
        const headers = {"Token": token};
        return axios.post('/events', event, {headers}).then(
            response => {
                dispatch(createProductSuccess());
                dispatch(push('/'));
            }
        );
    };
};

export const removeEvent = (id, userID) => {
    return dispatch => {
        axios.delete(`/events/${id}`).then(() => {
            NotificationManager.success('Событие успешно удалена', 'Успех!');
            dispatch(fetchProducts(userID));
        });
    };
};

export const searchUser = email => {
    return dispatch => {
        axios.post('/users/email', {email: email}).then(response => {
            if (response.data.username) {
                NotificationManager.success('Пользователь найден!', 'Успех!');
            } else {
                NotificationManager.error('Пользователь не найден!', 'Нет результатов!');
            }

            dispatch(shareWith(response.data));
        })
    }
};

export const share = (friend, token) => {
    const headers = {"Token": token};
    return dispatch => {
        axios.post('/users/share', friend, {headers}).then(response => {
            if (response.data.error) NotificationManager.error(response.data.error, "Ошибка!");
            dispatch({type: 'SUCCESS_SHARE', user: response.data});
            dispatch(push('/'));
        })
    }
};

export const unsubscribe = (id, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.delete(`users/unsubscribe/${id}`, {headers}).then(() => {
            dispatch(push('/'));
        })
    }
};