import {FETCH_PRODUCTS_SUCCESS, SHARE_WITH_USER} from "../actions/actionTypes";

const initialState = {
    products: [],
    shareWith: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        case SHARE_WITH_USER:
            return {...state, shareWith: action.user};
        default:
            return state;
    }
};

export default reducer;