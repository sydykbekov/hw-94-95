import axios from "../../axios-api";
import {put} from 'redux-saga/effects';
import {registerUserFailure, registerUserSuccess} from "../actions/users";
import {push} from "react-router-redux";
import {NotificationManager} from 'react-notifications';

export function* registerUserSaga(action) {
    try {
        yield axios.post('/users', action.userData);
        yield put(registerUserSuccess());
        yield put(push('/'));
        yield NotificationManager.success('Success', 'Registration success!');
    } catch (error) {
        yield put(registerUserFailure(error.response.data));
    }
}