const express = require('express');
const Event = require('../models/Event');

const auth = require('../middleware/auth');

const router = express.Router();

const createRouter = () => {
    router.get('/', auth, (req, res) => {
        const arr = [];
        const friends = req.user.subscribers;

        if (friends.length > 0) {
            friends.forEach(friend => {
                arr.push(Event.find({userID: friend._id}))
            });

            Promise.all(arr).then(results => {
                let array = [];

                if (results.length === 1) res.send(results);
                else {
                    for (let i = 0; i < results.length; i++) {
                        array = results[i].concat(array)
                    }
                }
                res.send(results);
            });
        } else {
            Event.find({userID: req.user._id}).then(result => {
                res.send(result);
            }).catch(() => res.sendStatus(500));
        }
    });

    router.post('/', auth, (req, res) => {
        const productData = req.body;
        productData.userID = req.user._id;

        const product = new Event(productData);

        product.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', (req, res) => {
        const id = req.params.id;
        Event.deleteOne({_id: id}).then(result => {
            res.send(result);
        })
    });

    return router;
};

module.exports = createRouter;