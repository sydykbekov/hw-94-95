const express = require('express');
const request = require('request-promise-native');

const User = require('../models/User');
const auth = require('../middleware/auth');
const config = require('../config');
const nanoid = require("nanoid");

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const user = new User({
            username: req.body.username,
            password: req.body.password
        });

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error))
    });

    router.post('/facebookLogin', async (req, res) => {
        const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${req.body.accessToken}&access_token=${config.facebook.appId}|${config.facebook.appSecret}`

        try {
            const response = await request(debugTokenUrl);

            const decodedResponse = JSON.parse(response);
            console.log(decodedResponse);

            if (decodedResponse.data.error) {
                return res.status(401).send({message: 'Facebook token incorrect'});
            }

            if (req.body.id !== decodedResponse.data.user_id) {
                return res.status(401).send({message: 'Wrong user ID'});
            }

            let user = await User.findOne({facebookId: req.body.id});

            if (!user) {
                user = new User({
                    username: req.body.email,
                    password: nanoid(),
                    facebookId: req.body.id,
                    displayName: req.body.name
                });

                await user.save();
            }

            let token = user.generateToken();

            return res.send({message: 'Login or register successful', user, token});

        } catch (error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }
    });

    router.post('/sessions', async (req, res) => {
        const user = await User.findOne({username: req.body.username});

        if (!user) {
            return res.status(400).send({error: 'Username not found'});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({error: 'Password is wrong!'});
        }

        const token = user.generateToken();

        return res.send({message: 'User and password correct!', user, token});
    });

    router.post('/verify', auth, (req, res) => {
        res.send({message: 'Token valid'});
    });

    router.delete('/sessions', async (req, res) => {
        const token = req.get('Token');
        const success = {message: 'Logout success!'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();
        await user.save();

        return res.send(success);
    });

    router.post('/email', (req, res) => {
        const email = req.body.email;

        User.findOne({email: email}).then(result => {
            res.send(result);
        }).catch(e => res.send(e));
    });

    router.post('/share', auth, async (req, res) => {
        let user = req.user;
        if (req.body._id.toString() === user._id.toString()) {
            res.send({error: 'Нельзя добавить себя в друзья!'});
        }
        user.subscribers.forEach(friend => {
            if (friend._id === req.body._id.toString()) return res.send({error: 'Этот пользователь уже в друзьях!'});
        });
        user.subscribers.push(req.body);
        await user.save().then(result => res.send(result));
    });

    router.delete('/unsubscribe/:id', auth, async (req, res) => {
        let user = req.user;
        let id = req.params.id;
        let index;
        user.subscribers.forEach((friend, key) => friend._id === id.toString() ? index = key : null);
        user.subscribers.splice(index, 1);
        await user.save().then(result => res.send(result));
    });

    return router;
};

module.exports = createRouter;