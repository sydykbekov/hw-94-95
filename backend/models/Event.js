const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EventSchema = new Schema({
    title: {
        type: String, required: true
    },
    date: {
        type: String, required: true
    },
    duration: {
        type: String, required: true
    },
    userID: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;